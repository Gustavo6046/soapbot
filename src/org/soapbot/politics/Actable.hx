package org.soapbot.politics;



interface Actable {
    public function describe(): String;
    public function revoke(): Bool;
    public function modify(action: Action): Bool;
}