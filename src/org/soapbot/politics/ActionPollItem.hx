package org.soapbot.politics;



class ActionPollItem extends ExpectedPollItem {
    public var action(default, null): Action;

    public function new(description: String, act: Action) {
        super(description);
        action = act;
    }

    override function epi_onWin(poll: Poll) {
        action.act();
    }
}