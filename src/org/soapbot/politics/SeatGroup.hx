package org.soapbot.politics;



import haxe.xml.Access;

class SeatGroup implements SeatContainer implements SeatItem {
    public var seats: Array<SeatItem> = new Array();

    public inline function new(seatType: Null<Class<SeatItem>> = null, numSeats: Int = 0, seatArguments: Null<Array<Dynamic>> = null) {
        if (seatArguments == null)
            seatArguments = new Array();

        if (seatType != null && numSeats > 0)
            for (i in 0...numSeats)
                seats.push(Type.createInstance(seatType, seatArguments));
    }

    public function length(): Int {
        var res = 0;

        for (seat in seats) {
            res += seat.length();
        }

        return res;
    }

    public function describe(): String {
        return "Seat group";
    }

    public function seatItemIterator(): Iterator<SeatItem> {
        return seats.iterator();
    }

    public function addSeatItem(newSeat: SeatItem): Bool {
        if (seats.indexOf(newSeat) != -1)
            return false;

        seats.push(newSeat);
        return true;
    }

    public function removeSeatItem(oldSeat: SeatItem): Bool {
        if (seats.indexOf(oldSeat) == -1)
            return false;

        seats.remove(oldSeat);
        return true;
    }

    public function numOccupants(): Int {
        var res = 0;

        for (seat in seats) {
            res += seat.numOccupants();
        }

        return res;
    }
}