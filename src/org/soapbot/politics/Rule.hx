package org.soapbot.politics;



interface Rule extends Actable {
    public function name(): String;
    public function content(): String;
    public function writingTime(): Date;
    public function getWriter(): Null<Individual>;
}