package org.soapbot.politics;



import haxe.ds.ObjectMap;

class Poll {
    public var participants(default, null): Array<Individual> = new Array();
    public var items(default, null):        ObjectMap<PollItem, Int> = new ObjectMap();


    public function new(participants: Null<Array<Individual>> = null, items: Null<Array<PollItem>> = null) {
        if (participants != null) {
            for (participant in participants)
                addParticipant(participant);
        }

        if (items != null) {
            for (item in items)
                addItem(item);
        }
    }

    public function addParticipant(participant: Individual) {
        if (participants.indexOf(participant) == -1) participants.push(participant);
    }

    public function addItem(item: PollItem) {
        items.set(item, 0);
    }

    public function castVote(individual: Individual, voteOn: PollItem): Bool {
        if (participants.indexOf(individual) == -1) return false;
        if (!items.exists(voteOn)) return false;

        items.set(voteOn, items.get(voteOn) + 1);
        
        voteOn.onVoted(this);

        return true;
    }

    public function end(): Null<PollItem> {
        if (!items.keys().hasNext()) return null;

        var winner: Null<PollItem> = null;
        var winnerVotes = 0;

        for (itm in items.keys()) {
            var vts = items.get(itm);
            
            if (winner == null || vts > winnerVotes) {
                winner = itm;
                winnerVotes = vts;
            }
        }

        if (winner != null) winner.onWin(this);
        
        return winner;
    }
}