package org.soapbot.politics;

import org.soapbot.futures.TaskLoop;
import org.soapbot.politics.actions.AddRuleAction;
import org.soapbot.politics.actions.RemoveRuleAction;
import org.soapbot.politics.actions.ActionOnIndividual;



class PoliticsSystem {
    public var taskLoop   (default, null): TaskLoop = new TaskLoop();

    public var legislators(default, null): SeatGroup;
    public var  moderators(default, null): SeatGroup;

    public var people     (default, null): Array<Individual> = new Array();
    
    var        legisPolls: PollSystem = new PollSystem();
    var        moderPolls: PollSystem = new PollSystem();

    public var rules      (default, null): Array<Rule> = new Array();

    public function new(numLegisSeats: Int = 8, numModerSeats: Int = 5) {
        var na: Array<Null<Individual>> = new Array();
        na.push(null);

        legislators = new SeatGroup(Seat, numLegisSeats, na);
        moderators  = new SeatGroup(Seat, numModerSeats, na);
    }

    public function addPerson(person: Individual) {
        if (people.indexOf(person) == -1) people.push(person);
    }

    public function removePerson(person: Individual) {
        if (people.indexOf(person) != -1) people.remove(person);
    }

    function updateTasks(deltaTime: Float = 0) {
        taskLoop.runStep(deltaTime);
    }

    public function voteAddRule(rule: Rule) {
        if (rules.indexOf(rule) == -1)
            legisPolls.pollOn(new AddRuleAction(this, rule));
    }

    public function voteRemoveRule(rule: Rule) {
        if (rules.indexOf(rule) == -1)
            legisPolls.pollOn(new RemoveRuleAction(this, rule));
    }

    public function voteOntoPerson(person: Individual, verb: String, whatDo: (person: Individual) -> Void) {
        moderPolls.pollOn(new ActionOnIndividual(person, verb, whatDo));
    }

    public function addRule(rule: Rule) {
        if (rules.indexOf(rule) == -1) rules.push(rule);
    }

    public function removeRule(rule: Rule) {
        if (rules.indexOf(rule) != -1) rules.remove(rule);
    }
}