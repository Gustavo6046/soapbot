package org.soapbot.politics;



interface Individual extends Actable {
    public function name(): String;

    public function onRemovedFromSeat(from: Seat): Void;
    public function onAppointedToSeat(to:   Seat): Void;
}