package org.soapbot.politics;



enum ActionType {
    AT_NONE;
    AT_CREATE;
    AT_REVOKE;
    AT_MODIFY;
    AT_UNKNOWN;
}