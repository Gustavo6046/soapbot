package org.soapbot;



class SimpleQueue<T> {
    var inbox: Array<T> = new Array();
    var outbox: Array<T> = new Array();

    public inline function new() {}

    public function put(item: T) {
        inbox.push(item);
    }

    public inline function size(): Int {
        return inbox.length + outbox.length;
    }

    public inline function empty(): Bool {
        return inbox.length == 0 && outbox.length == 0;
    }

    public function get(): T {
        if (outbox.length == 0) {
            while (inbox.length > 0)
                outbox.push(inbox.pop());
        }

        return outbox.pop();
    }
}