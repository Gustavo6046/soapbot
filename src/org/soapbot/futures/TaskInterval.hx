package org.soapbot.futures;



class TaskInterval extends TaskTimer {
    override public function timeout() {
        this.loop.enqueue(task);
        this.threshold = interval;
    }
}