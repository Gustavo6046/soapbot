package org.soapbot.futures;



class ExternalTimer extends TaskTimer {
    public var isSet(default, null): Bool = false;

    public function set(): Void {
        isSet = true;
    }

    public function new(loop: TaskLoop, task: Task) {
        super(loop, task, 0);
    }

    override public function stepSelf(_deltaTime: Float) {
        if (isSet) {
            timeout();
        }
    }
}