package org.soapbot.futures;

import org.soapbot.SimpleQueue;



class TaskLoop {
    var queue: SimpleQueue<Task> = new SimpleQueue();
    var timers: Array<TaskTimer> = new Array();

    public inline function new() {}

    public function stepQueueOne() {
        if (queue.empty()) return;

        var task = queue.get();
        task.performTask(this);
    }

    public function stepQueueAll() {
        while (!queue.empty()) {
            var task = queue.get();
            task.performTask(this);
        }
    }

    public function enqueue(task: Task) {
        queue.put(task);
    }

    public function removeTimer(timer: TaskTimer) {
        timers.remove(timer);
    }

    public function stepTimers(deltaTime: Float) {
        for (t in timers) {
            t.stepSelf(deltaTime);
        }
    }

    public function runStep(deltaTime: Float) {
        stepTimers(deltaTime);
        stepQueueAll();
    }

    public function setTimeout(task: Task, interval: Float) {
        var timer = new TaskTimer(this, task, interval);
        timers.push(timer);
    }

    public function setInterval(task: Task, interval: Float) {
        var timer = new TaskInterval(this, task, interval);
        timers.push(timer);
    }

    public function createWaiter(task: Task): ExternalTimer {
        var timer = new ExternalTimer(this, task);
        timers.push(timer);
        return timer;
    }
}