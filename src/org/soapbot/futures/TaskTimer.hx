package org.soapbot.futures;



class TaskTimer {
    public var threshold(default, null): Float;
    public var interval (default, null): Float;
    
    var task: Task;
    var loop: TaskLoop;

    public function new(loop: TaskLoop, task: Task, after: Float) {
        this.task = task;
        threshold = after;
        interval = threshold;
        this.loop = loop;
    }

    public function stepSelf(deltaTime: Float) {
        threshold -= deltaTime;

        if (threshold <= 0) {
            timeout();
        }
    }

    public function timeout() {
        this.loop.enqueue(task);
        removeSelf();
    }

    public function removeSelf() {
        loop.removeTimer(this);
    }
}